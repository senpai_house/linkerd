FROM ubuntu:20.04
RUN apt update && \
    apt install -y wget curl && \
    curl --proto '=https' --tlsv1.2 -sSfL https://run.linkerd.io/install | sh && \
    echo 'export PATH=$PATH:/root/.linkerd2/bin' >> ~/.bashrc && \
    wget https://dl.k8s.io/release/v1.23.0/bin/linux/amd64/kubectl -O /usr/bin/kubectl && \
    chmod +x /usr/bin/kubectl && \
    mkdir /tmp/doctl && cd /tmp/doctl && \
    wget https://github.com/digitalocean/doctl/releases/download/v1.70.0/doctl-1.70.0-linux-amd64.tar.gz -O /tmp/doctl/doctl-1.70.0-linux-amd64.tar.gz && \
    tar xf /tmp/doctl/doctl-1.70.0-linux-amd64.tar.gz && \
    mv /tmp/doctl/doctl /usr/bin/doctl
